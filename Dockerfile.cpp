FROM ubuntu:18.04 AS builder

RUN apt-get update && apt-get install -y clang git wget build-essential libssl-dev

ENV CMAKE_VERSION=3.18.1
RUN wget https://github.com/Kitware/CMake/releases/download/v${CMAKE_VERSION}/cmake-${CMAKE_VERSION}.tar.gz
RUN tar -xzf cmake-${CMAKE_VERSION}.tar.gz
RUN cd cmake-${CMAKE_VERSION} && ./bootstrap && make && make install
RUN cmake --version

ENV CC=/usr/bin/clang
ENV CXX=/usr/bin/clang++
